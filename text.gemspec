#########################################################
# This file has been automatically generated by gem2tgz #
#########################################################
# -*- encoding: utf-8 -*-
# stub: text 1.3.1 ruby lib

Gem::Specification.new do |s|
  s.name = "text".freeze
  s.version = "1.3.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Paul Battley".freeze, "Michael Neumann".freeze, "Tim Fletcher".freeze]
  s.date = "2015-04-13"
  s.description = "A collection of text algorithms: Levenshtein, Soundex, Metaphone, Double Metaphone, Porter Stemming".freeze
  s.email = "pbattley@gmail.com".freeze
  s.extra_rdoc_files = ["COPYING.txt".freeze, "README.rdoc".freeze]
  s.files = ["COPYING.txt".freeze, "README.rdoc".freeze, "Rakefile".freeze, "lib/text.rb".freeze, "lib/text/double_metaphone.rb".freeze, "lib/text/levenshtein.rb".freeze, "lib/text/metaphone.rb".freeze, "lib/text/porter_stemming.rb".freeze, "lib/text/soundex.rb".freeze, "lib/text/version.rb".freeze, "lib/text/white_similarity.rb".freeze, "test/data/double_metaphone.csv".freeze, "test/data/metaphone.yml".freeze, "test/data/metaphone_buggy.yml".freeze, "test/data/porter_stemming_input.txt".freeze, "test/data/porter_stemming_output.txt".freeze, "test/data/soundex.yml".freeze, "test/double_metaphone_test.rb".freeze, "test/levenshtein_test.rb".freeze, "test/metaphone_test.rb".freeze, "test/porter_stemming_test.rb".freeze, "test/soundex_test.rb".freeze, "test/test_helper.rb".freeze, "test/text_test.rb".freeze, "test/white_similarity_test.rb".freeze]
  s.homepage = "http://github.com/threedaymonk/text".freeze
  s.licenses = ["MIT".freeze]
  s.rubyforge_project = "text".freeze
  s.rubygems_version = "2.5.2.1".freeze
  s.summary = "A collection of text algorithms".freeze

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<rake>.freeze, ["~> 10.0"])
    else
      s.add_dependency(%q<rake>.freeze, ["~> 10.0"])
    end
  else
    s.add_dependency(%q<rake>.freeze, ["~> 10.0"])
  end
end
